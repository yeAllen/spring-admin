package service;

import java.util.List;

import dto.ComplaintDto;
import dto.ResponseDto;
import pojo.Complaint;

public interface ComplaintService {

	public Complaint add(ComplaintDto complaint);
	public List<Complaint> getAll();
	public ResponseDto getByStatus(String status);
	public Complaint updateStatus(ComplaintDto complaint);
}
