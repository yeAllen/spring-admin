package service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import dao.ComplaintDao;
import dao.MerchantDao;
import dto.ComplaintDto;
import dto.ResponseDto;
import dto.StatusDto;
import pojo.Complaint;
import pojo.Merchant;
import service.ComplaintService;
import util.JerseyClient;
import util.JsonUtil;
import util.PropertiesUtils;

@Service(value="complaintService")
public class ComplaintServiceImpl implements ComplaintService{

	protected Logger logger = LogManager.getLogger(this.getClass());
	@Resource(name="complaintDao")
	private ComplaintDao cDao;
	@Resource(name="merchantDao")
	private MerchantDao merchantDao;
	JerseyClient client = null;
	
	public ComplaintServiceImpl(){
		client = new JerseyClient();
	}
	
	@Override
	public List<Complaint> getAll() {
		return cDao.findAll();
	}

	@Override
	public ResponseDto getByStatus(String status) {
		List<ComplaintDto> complaintDtos = new ArrayList<>();
		List<Complaint> complaints = cDao.findByStatus(status);
		if(complaints.size() > 0){
			for(Complaint c:complaints){
				Merchant merchant = c.getMerchant();
				String mName = "";
				if(merchant != null)
					mName = merchant.getName();
				ComplaintDto cDto = new ComplaintDto(c.getCname(), c.getComplaintContext(), 
						c.getComplaintDate(), mName, c.getComplaintStatus(), c.getId());
				complaintDtos.add(cDto);
			}
			ResponseDto responseDto = new ResponseDto(1, "success", complaintDtos);
			return responseDto;
		}
		return null;
	}

	@Override
	@Transactional
	public Complaint updateStatus(ComplaintDto complaint) {
		try {
			List<Merchant> merchant = merchantDao.getByName(complaint.getmName());
			if(merchant.size() > 0){
				Complaint complaint2 = new Complaint(complaint.getId(), complaint.getcName(), complaint.getComplaintContext(), complaint.getStatus(), complaint.getComplaintDate(), merchant.get(0));
				if(Complaint.DANGER.equals(complaint2.getComplaintStatus())){
					Merchant merchant2 = merchantDao.updateStatusByName(merchant.get(0).getName(), Merchant.BLACK_LIST);
					
					StatusDto statusDto = new StatusDto(merchant2.getName(), merchant2.getStatus(), "change");
					client.post(PropertiesUtils.getProperties("merchant.post.status"), JsonUtil.toJsonString(statusDto));
					
				}
				return cDao.update(complaint2);
			}
			return null;
		} catch (Exception e) {
			logger.error(e);
			return null;
		}
		
	}

	@Override
	@Transactional
	public Complaint add(ComplaintDto complaint) {
		try {
			List<Merchant> merchants = merchantDao.getByName(complaint.getmName());
			if(merchants.size() > 0){
				Complaint complaint2 = new Complaint(null, complaint.getcName(), 
						complaint.getComplaintContext(), Complaint.UNHANDLE, complaint.getComplaintDate(), merchants.get(0));
				return cDao.add(complaint2);
			}
			return null;
		} catch (Exception e) {
			logger.error(e);
			return null;
		}
	}

}
