package service.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pojo.Admin;
import service.AdminService;
import dao.AdminDao;

@Service(value="adminService")
public class AdminServiceImpl implements AdminService {
	
	@Resource(name="adminDao")
	private AdminDao adminDao;

	@Override
	public Admin login(String name, String password) {
		return adminDao.findByName(name, password);
	}

	@Override
	@Transactional
	public void regist(Admin admin) {
		adminDao.add(admin);
	}
	
}
