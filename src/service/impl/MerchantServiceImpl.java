package service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pojo.Merchant;
import service.MerchantService;
import util.JerseyClient;
import util.JsonUtil;
import util.PropertiesUtils;
import vo.MerchantVO;
import dao.MerchantDao;
import data.VersionTable;
import dto.MerchantChangeDto;
import dto.ResponseDto;
import dto.StatusDto;
import dto.UpdateMerchatStatusDto;

@Service(value="merchantService")
public class MerchantServiceImpl implements MerchantService{
	
	protected Logger logger = LogManager.getLogger(this.getClass());
	@Resource(name="merchantDao")
	private MerchantDao merchantDao;
	@Resource(name="versionTable")
	private VersionTable versionTable;
	JerseyClient client = null;
	
	public MerchantServiceImpl(){
		client = new JerseyClient();
	}
	@Override
	@Transactional
	public Merchant add(Merchant merchant) {
		try {
			merchantDao.add(merchant);
			return merchant;
		} catch (Exception e) {
			logger.error(e);
			return null;
		}
		
	}
	
	@Override
	public List<MerchantVO> getAll() {
		List<MerchantVO> mVos = new ArrayList<>();
		List<Merchant> merchants = merchantDao.findAll();
		for(Merchant m:merchants){
			mVos.add(new MerchantVO(m.getName(), m.getIdCard(), m.getAvatar(), m.getAddress(), m.getStatus()));
		}
		return mVos;
	}
	
	@Override
	public Merchant get(String id) {
		return null;
	}
	
	@Override
	public List<Merchant> getByStatus(String status) {
		return merchantDao.findByStatus(status);
	}
	
	@Override
	public List<Merchant> getByName(String name) {
		return merchantDao.getByName(name);
	}
	
	@Override
	@Transactional
	public ResponseDto updateStatusByName(String name, String operation) {
		ResponseDto responseDto = new ResponseDto();
		try {
			Merchant merchant = merchantDao.updateStatusByName(name, operation);
			responseDto.setCode(1);
			responseDto.setMessage("success");
			responseDto.setData(merchant);
			return responseDto;
		} catch (Exception e) {
			logger.error(e);
			responseDto.setCode(-1);
			responseDto.setMessage("illegal operation");
			return responseDto;
		}
	}
	
	@Override
	@Transactional
	public Merchant updateByName(Merchant merchant) {
		return merchantDao.updateByName(merchant);
	}
	
	@Override
	@Transactional
	public String audit(UpdateMerchatStatusDto merchant) {
		List<Merchant> merchant_db = merchantDao.getByName(merchant.getName());
		if (merchant_db != null && merchant_db.get(0).getUpdateTime()!=null 
				&& String.valueOf(merchant_db.get(0).getUpdateTime().getTime()).equals(merchant.getVersion())) {
			if (Merchant.WAIT_AUDIT.equals(merchant_db.get(0).getStatus()) && Merchant.WHITE_LIST.equals(merchant.getOperation())) {
				merchantDao.updateStatusByName(merchant.getName(), Merchant.WHITE_LIST);
				versionTable.put(VersionTable.AUDIT_VERSION, System.currentTimeMillis());
				return "success";
			}
			if( Merchant.REJECT.equals(merchant.getOperation())){
				if (!Merchant.WAIT_AUDIT.equals(merchant_db.get(0).getStatus())) {
					return "illegal change";
				}
				if("".equals(merchant.getRejectReason())){
					return "reson can not empty";
				}
				merchant_db.get(0).setStatus(Merchant.REJECT);
				merchant_db.get(0).setRejectReason(merchant.getRejectReason());
				merchantDao.updateByName(merchant_db.get(0));
				versionTable.put(VersionTable.AUDIT_VERSION, System.currentTimeMillis());
				return "success";
			}
			return "operation not allow";
		}
		return "data is update";
	}
}
