package service.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import dao.AdSpaceDao;
import dao.MerchantDao;
import data.AdSpaceTimeStamp;
import dto.AdSpaceDto;
import dto.AdSpaceToCDto;
import dto.AdSpaceToMDto;
import dto.ResponseDto;
import pojo.AdSpace;
import pojo.AdSpaceStatusEnum;
import pojo.Merchant;
import service.AdSpaceService;
import util.ParserDateFormatUtil;
import util.PropertiesUtils;

@Service(value="adSpaceService")
public class AdSpaceServiceImpl implements AdSpaceService{

	protected Logger logger = LogManager.getLogger(this.getClass());
	@Resource(name="adSpaceDao")
	private AdSpaceDao aDao;
	@Resource(name="merchantDao")
	private MerchantDao mDao;
	ParserDateFormatUtil paUtil = null;
	
	public AdSpaceServiceImpl(){
		paUtil = new ParserDateFormatUtil();
	}
	
	@Override
	@Transactional
	public AdSpace add(AdSpaceDto adSpace) {
		try {
			List<Merchant> merchants = mDao.getByName(adSpace.getmName());
			if(merchants != null){
				AdSpace aSpace = new AdSpace(null, adSpace.getId(), adSpace.getImgPath(),
						adSpace.getAdPrice(), adSpace.getFoodId(), adSpace.getStartTime(),
						adSpace.getEndTime(), AdSpace.DEFAULT, merchants.get(0), adSpace.getMid());
				AdSpace aSpace2 = aDao.add(aSpace);
				if(aSpace2 != null){
					return aSpace2;
				}
			}
			return null;
		} catch (Exception e) {
			logger.error(e);
			return null;
		}
		
	}

	@Override
	@Transactional
	public AdSpace updateStaus(AdSpaceDto adSpace) {
		AdSpaceTimeStamp aStamp = new AdSpaceTimeStamp();
		SimpleDateFormat dFormat = new SimpleDateFormat("HH:mm:ss");
		String startTime = dFormat.format(adSpace.getStartTime());
		//if timeStamp legal
		long isBehindTime = new Date().getTime() - adSpace.getStartTime().getTime();
		String endTime = aStamp.timeStampMap.get(startTime);
		//if can pass the new one
		List<AdSpace> adSpaces = aDao.getByTimeStamp(adSpace.getStartTime(), AdSpace.PASS);
		int sum = Integer.valueOf(PropertiesUtils.getProperties("adspace.timestamp.sum"));
//		if(isBehindTime < 0 && endTime != null && adSpaces.size() < sum){
			if(endTime != null && adSpaces.size() < sum){
			try {
				//adSpace.setStatus(AdSpace.PASS);
				List<Merchant> merchant = mDao.getByName(adSpace.getmName());
				AdSpace ad = new AdSpace(adSpace.getKey(), adSpace.getId(), 
						adSpace.getImgPath(), adSpace.getAdPrice(), adSpace.getFoodId(),
						adSpace.getStartTime(), adSpace.getEndTime(), adSpace.getStatus(),
						merchant.get(0), adSpace.getMid());
				AdSpace adSpace2 = aDao.update(ad);
				if(adSpace2 != null){
					return adSpace2;
				}
				return null;
			} catch (Exception e) {
				logger.error(e);
				return null;
			}
		}
		return null;
	}

	@Override
	public ResponseDto getByStatus(String status) {
		List<AdSpaceDto> aDtos = new ArrayList<>();
		List<AdSpace> adSpaces = aDao.getByStatus(status);
		if(adSpaces.size() > 0){
			for(AdSpace a:adSpaces){
				Merchant merchant = a.getMerchant();
				String mName = "";
				if(merchant != null)
					mName = merchant.getName();
				//Date startTimeDate = paUtil.parser(a.getStartTime(), "yyyy-MM-dd HH:mm:ss");
				//Date endTimeDate = paUtil.parser(a.getEndTime(), "yyyy-MM-dd HH:mm:ss");
				AdSpaceDto aDto = new AdSpaceDto(a.getReqId(), PropertiesUtils.getProperties("image.get.path")+a.getImgPath(), a.getAdPrice(), 
						a.getFoodId(), a.getStartTime(), a.getEndTime(), mName, a.getMid(),
						a.getStatus(),a.getId());
				System.out.println(aDto.toString());
				aDtos.add(aDto);
			}
			ResponseDto responseDto = new ResponseDto(1, "success", aDtos);
			return responseDto;
		}
		return null;
	}
	
	@Override
	public List<AdSpaceToCDto> getPassInTime(String status, Date time) {
		int sum = Integer.valueOf(PropertiesUtils.getProperties("adspace.timestamp.sum"));
		List<AdSpaceToCDto> aCDtos = new ArrayList<>();
		
		SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String ss = s.format(time);
		Date time2 = null;
		try {
			time2 = s.parse(ss);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		List<AdSpace> adSpaces = aDao.getAdsInOneTime(time2, status);
		for(AdSpace a:adSpaces){
			AdSpaceToCDto aCDto = new AdSpaceToCDto(a.getImgPath(), a.getFoodId(), a.getMid());
			System.out.println("fto...."+aCDto.toString());
			aCDtos.add(aCDto);
		}
		if(aCDtos.size() < sum){
			List<AdSpace> adSpaces_default = aDao.getByStatus(AdSpace.SUBSTITUTION);
			if(adSpaces_default.size() > 0){
				int defaultNum = sum - aCDtos.size();
				for(int i =0; i<defaultNum; i++){
					AdSpaceToCDto aCDto2 = new AdSpaceToCDto(adSpaces_default.get(i).getImgPath(), null, null);
					aCDtos.add(aCDto2);
				}
			}
		}
		return aCDtos;
	}

	@Override
	public AdSpaceToMDto getById(String id) {
		AdSpace adSpace = aDao.load(id);
		AdSpaceToMDto aMDto = new AdSpaceToMDto(adSpace.getReqId(), adSpace.getStatus());
		return aMDto;
	}

	@Override
	public AdSpace getAllById(String id) {
		return aDao.load(id);
	}

}
