package service;

import java.util.Date;
import java.util.List;

import dto.AdSpaceDto;
import dto.AdSpaceToCDto;
import dto.AdSpaceToMDto;
import dto.ResponseDto;
import pojo.AdSpace;
import pojo.AdSpaceStatusEnum;

public interface AdSpaceService {

	public AdSpace add(AdSpaceDto adSpace);
	public AdSpace updateStaus(AdSpaceDto adSpace);
	public List<AdSpaceToCDto> getPassInTime(String status, Date time);
	public ResponseDto getByStatus(String status);
	public AdSpaceToMDto getById(String id);
	public AdSpace getAllById(String id);
}
