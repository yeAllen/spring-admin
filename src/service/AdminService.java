package service;

import pojo.Admin;

public interface AdminService {
	
	public void regist(Admin admin);
	public Admin login(String name, String password);
}
