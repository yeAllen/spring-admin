package service;

import java.util.List;

import dto.ResponseDto;
import dto.UpdateMerchatStatusDto;
import pojo.Merchant;
import vo.MerchantVO;

public interface MerchantService {
	public Merchant add(Merchant merchant);
	
	public List<MerchantVO> getAll();
	
	public Merchant get(String id);
	
	public List<Merchant> getByStatus(String status);
	
	public List<Merchant> getByName(String name);
	
	public ResponseDto updateStatusByName(String name,String operation);
	
	public Merchant updateByName(Merchant merchant);
	
	public String audit(UpdateMerchatStatusDto merchant);
		
}
