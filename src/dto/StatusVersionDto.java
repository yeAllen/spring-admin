package dto;

public class StatusVersionDto {
	
	private long version;
	
	

	public StatusVersionDto() {
		super();
	}

	public StatusVersionDto(long version) {
		super();
		this.version = version;
	}

	public long getVersion() {
		return version;
	}

	public void setVersion(long version) {
		this.version = version;
	}
	
	

}
