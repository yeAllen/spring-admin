package dto;

import java.util.Date;

import util.MyJsonSeriallizer;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * RECEIVE FROM M
 * RECEIVE FROM RENDER_ADS.XML
 * @author ZHENGNI3
 *
 */
public class AdSpaceDto {

	private String id;
	//@JsonSerialize(using=MyJsonSeriallizer.class)
	private String imgPath;
	private double adPrice;
	private String foodId;
	private Date startTime;
	private Date endTime;
	private String mName;
	private String mid;
	
	//RECEIVE FROM RENDER_ADS.XML
	private String status;
	private String key;
	
	public AdSpaceDto(){}
	public AdSpaceDto(String id, double adPrice) {
		super();
		this.id = id;
		this.adPrice = adPrice;
	}
	public AdSpaceDto(String id, String imgPath, double adPrice, String foodId,
			Date startTime, Date endTime, String mName, String mid) {
		super();
		this.id = id;
		this.imgPath = imgPath;
		this.adPrice = adPrice;
		this.foodId = foodId;
		this.startTime = startTime;
		this.endTime = endTime;
		this.mName = mName;
		this.mid = mid;
	}
	public AdSpaceDto(String id, String imgPath, double adPrice, String foodId,
			Date startTime, Date endTime, String mName, String mid,
			String status, String key) {
		super();
		this.id = id;
		this.imgPath = imgPath;
		this.adPrice = adPrice;
		this.foodId = foodId;
		this.startTime = startTime;
		this.endTime = endTime;
		this.mName = mName;
		this.mid = mid;
		this.status = status;
		this.key = key;
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getImgPath() {
		return imgPath;
	}
	public void setImgPath(String imgPath) {
		this.imgPath = imgPath;
	}
	public double getAdPrice() {
		return adPrice;
	}
	public void setAdPrice(double adPrice) {
		this.adPrice = adPrice;
	}
	public String getFoodId() {
		return foodId;
	}
	public void setFoodId(String foodId) {
		this.foodId = foodId;
	}
	public Date getStartTime() {
		return startTime;
	}
	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}
	public Date getEndTime() {
		return endTime;
	}
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}
	public String getmName() {
		return mName;
	}
	public void setmName(String mName) {
		this.mName = mName;
	}
	public String getMid() {
		return mid;
	}
	public void setMid(String mid) {
		this.mid = mid;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	@Override
	public String toString() {
		return "AdSpaceDto [id=" + id + ", imgPath=" + imgPath + ", adPrice="
				+ adPrice + ", foodId=" + foodId + ", startTime=" + startTime
				+ ", endTime=" + endTime + ", mName=" + mName + ", mid=" + mid
				+ ", status=" + status + ", key=" + key + "]";
	}
	

}
