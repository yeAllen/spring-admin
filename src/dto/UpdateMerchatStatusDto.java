package dto;
/**
 * THE OBJECT FROM HTML WHEN AUDIT
 * @author ZHENGNI3
 *
 */
public class UpdateMerchatStatusDto {
	
	private String name;
	private String operation;
	private String rejectReason;
	private String status;
	private String version;
	
	public UpdateMerchatStatusDto() {
		super();
	}
	public UpdateMerchatStatusDto(String name, String operation) {
		super();
		this.name = name;
		this.operation = operation;
	}
	public UpdateMerchatStatusDto(String name, String operation,
			String rejectReason,String status,String version) {
		super();
		this.name = name;
		this.operation = operation;
		this.rejectReason = rejectReason;
		this.status = status;
		this.version = version;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getOperation() {
		return operation;
	}
	public void setOperation(String operation) {
		this.operation = operation;
	}
	public String getRejectReason() {
		return rejectReason;
	}
	public void setRejectReason(String rejectReason) {
		this.rejectReason = rejectReason;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	@Override
	public String toString() {
		return "UpdateMerchatStatusDto [name=" + name + ", operation="
				+ operation + ", rejectReason=" + rejectReason + ", status="
				+ status + ", version=" + version + "]";
	}
	
	
	

}
