package dto;

import util.MyJsonSeriallizer;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
/**
 * RESPONSE TO M
 * @author ZHENGNI3
 *
 */
public class MerchantDto {
	
	private String name;
	@JsonSerialize(using=MyJsonSeriallizer.class)
	private String avatar;
	private String address;
	
	public MerchantDto(){}
	public MerchantDto(String name, String avatar, String address) {
		super();
		this.name = name;
		this.avatar = avatar;
		this.address = address;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAvatar() {
		return avatar;
	}
	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
}
