package dto;

public class MerchantChangeDto {

	private String status;

	public MerchantChangeDto(){}
	public MerchantChangeDto(String status) {
		super();
		this.status = status;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
}
