package dto;

import util.MyJsonSeriallizer;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * RESPONSE TO C
 * @author ZHENGNI3
 *
 */
public class AdSpaceToCDto {

	@JsonSerialize(using=MyJsonSeriallizer.class)
	private String imgPath;
	private String foodId;
	private String mid;
	
	public AdSpaceToCDto(){}
	public AdSpaceToCDto(String imgPath, String foodId, String mid) {
		super();
		this.imgPath = imgPath;
		this.foodId = foodId;
		this.mid = mid;
	}

	public String getImgPath() {
		return imgPath;
	}
	public void setImgPath(String imgPath) {
		this.imgPath = imgPath;
	}
	public String getFoodId() {
		return foodId;
	}
	public void setFoodId(String foodId) {
		this.foodId = foodId;
	}
	public String getMid() {
		return mid;
	}
	public void setMid(String mid) {
		this.mid = mid;
	}
	@Override
	public String toString() {
		return "AdSpaceToCDto [imgPath=" + imgPath + ", foodId=" + foodId
				+ ", mid=" + mid + "]";
	}
}
