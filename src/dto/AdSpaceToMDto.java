package dto;

import util.MyJsonSeriallizer;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * RESPONSE TO M
 * @author ZHENGNI3
 *
 */
public class AdSpaceToMDto {

	private String adsId;
	private String status;
	
	public AdSpaceToMDto(){}
	public AdSpaceToMDto(String adsId, String status) {
		super();
		this.adsId = adsId;
		this.status = status;
	}

	public String getAdsId() {
		return adsId;
	}

	public void setAdsId(String adsId) {
		this.adsId = adsId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	@Override
	public String toString() {
		return "AdSpaceToMDto [adsId=" + adsId + ", status=" + status + "]";
	}
	
}
