package dto;

public class StatusDto {
	
	public final static String WAIT_AUDIT = "WAIT_AUDIT";
	public final static String SUCCESS = "SUCCESS";
	public final static String REJECT = "REJECT";
	public final static String BLACK_LIST = "BLACK_LIST";
	public final static String WHITE_LIST = "WHITE_LIST";
	
	private String merchantName;
	private String status;
	private String message;

	public StatusDto(){}
	public StatusDto(String merchantName, String status, String message) {
		super();
		this.merchantName = merchantName;
		this.status = status;
		this.message = message;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	public String getMerchantName() {
		return merchantName;
	}
	public void setMerchantName(String merchantName) {
		this.merchantName = merchantName;
	}
	@Override
	public String toString() {
		return "StatusDto [merchantName=" + merchantName + ", status=" + status
				+ ", message=" + message + "]";
	}
	
}
