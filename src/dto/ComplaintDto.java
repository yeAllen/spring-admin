package dto;

import java.util.Date;
/**
 * RECEIVE FORM C
 * @author ZHENGNI3
 *
 */
public class ComplaintDto {

	private String cName;
	private String complaintContext;
	private Date complaintDate;
	private String mName;
	//PACKING FOR RENDER_COMPLAINT.XML
	private String status;
	private String id;
	
	public ComplaintDto(){}
	public ComplaintDto(String cName, String complaintContext,
			Date complaintDate, String mName) {
		super();
		this.cName = cName;
		this.complaintContext = complaintContext;
		this.complaintDate = complaintDate;
		this.mName = mName;
	}
	public ComplaintDto(String cName, String complaintContext,
			Date complaintDate, String mName, String status, String id) {
		super();
		this.cName = cName;
		this.complaintContext = complaintContext;
		this.complaintDate = complaintDate;
		this.mName = mName;
		this.status = status;
		this.id = id;
	}
	
	public String getcName() {
		return cName;
	}
	public void setcName(String cName) {
		this.cName = cName;
	}
	public String getComplaintContext() {
		return complaintContext;
	}
	public void setComplaintContext(String complaintContext) {
		this.complaintContext = complaintContext;
	}
	public Date getComplaintDate() {
		return complaintDate;
	}
	public void setComplaintDate(Date complaintDate) {
		this.complaintDate = complaintDate;
	}
	public String getmName() {
		return mName;
	}
	public void setmName(String mName) {
		this.mName = mName;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	@Override
	public String toString() {
		return "ComplaintDto [cName=" + cName + ", complaintContext="
				+ complaintContext + ", complaintDate=" + complaintDate
				+ ", mName=" + mName + "]";
	}
	
}
