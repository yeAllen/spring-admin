package controller;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import dto.MerchantChangeDto;
import dto.MerchantDto;
import dto.ResponseDto;
import dto.StatusDto;
import dto.UpdateMerchatStatusDto;
import pojo.Merchant;
import service.MerchantService;
import util.JerseyClient;
import util.JsonUtil;
import util.PropertiesUtils;
import vo.MerchantVO;

@Controller
public class MerchantController {

	@Resource(name="merchantService")
	private MerchantService mService;
	JerseyClient client = null;
	
	public MerchantController(){
		client = new JerseyClient();
	}
	
	@ResponseBody
	@RequestMapping(value="/merchant", method=RequestMethod.GET)
	public List<MerchantVO> getAll(){
		return mService.getAll();
	}
	
	@ResponseBody
	@RequestMapping(value="/merchant/{status}", method=RequestMethod.GET)
	public ResponseDto getByStatus(@PathVariable String status){
		List<Merchant> merchants = mService.getByStatus(status);
		List<MerchantDto> merchantDtos = new ArrayList<>();
		for (Merchant m:merchants) {
			MerchantDto mDto = new MerchantDto(m.getName(), m.getAvatar(), m.getAddress());
			merchantDtos.add(mDto);
		}
		ResponseDto rDto = new ResponseDto(1,"success",merchantDtos);
		return rDto;
//		return mService.getByStatus(status);
	}
	
	@ResponseBody
	@RequestMapping(value="/merchant/list/{status}", method=RequestMethod.GET)
	public ResponseDto getByStatus2(@PathVariable String status){
		List<Merchant> merchants = mService.getByStatus(status);
		for (Merchant m:merchants) {
			m.setIdCard(PropertiesUtils.getProperties("image.get.path")+m.getIdCard());
			m.setAvatar(PropertiesUtils.getProperties("image.get.path")+m.getAvatar());
		}
		ResponseDto rDto = new ResponseDto(1,"success",merchants);
		return rDto;
//		return mService.getByStatus(status);
	}
	
	@ResponseBody
	@RequestMapping(value="/merchant/name/{name}", method=RequestMethod.GET)
	public ResponseDto getByName(@PathVariable String name){
		List<Merchant> merchant = mService.getByName(name);
		for(Merchant m:merchant){
			m.setAvatar(PropertiesUtils.getProperties("image.get.path")+m.getAvatar());
			m.setIdCard(PropertiesUtils.getProperties("image.get.path")+m.getIdCard());
		}
		ResponseDto rDto = new ResponseDto(1, "success", merchant);
		return rDto;
	}
	
	@ResponseBody
	@RequestMapping(value="/merchant/audit", method=RequestMethod.PUT)
	public ResponseDto audit(@RequestBody UpdateMerchatStatusDto user){
		ResponseDto responseDto = new ResponseDto();
		if (user==null||"".equals(user)) {
			responseDto.setCode(-1);
			responseDto.setMessage("message can not be null");
			
		}else{
			String result = mService.audit(user);
			if ("success".equals(result)) {
				responseDto.setCode(1);
				responseDto.setMessage("success");
				//info M/C
				MerchantChangeDto mChangeDto = new MerchantChangeDto("change");
				client.post(PropertiesUtils.getProperties("customer.post.status"), JsonUtil.toJsonString(mChangeDto));
				StatusDto statusDto = new StatusDto(user.getName(), user.getOperation().toLowerCase(), "change");
				client.post(PropertiesUtils.getProperties("merchant.post.status"), JsonUtil.toJsonString(statusDto));
			}else{
				responseDto.setCode(-1);
				responseDto.setMessage(result);
			}
		}
		return responseDto;
	}
	
	@ResponseBody
	@RequestMapping(value="/merchant/status", method=RequestMethod.PUT)
	public ResponseDto updateStatus(@RequestBody UpdateMerchatStatusDto user){
		ResponseDto responseDto = new ResponseDto();
		responseDto.setCode(-1);
		if (user==null) {
			responseDto.setMessage("illegal data");
			return responseDto;
		}
		boolean errorInput = false;
		if (user.getName()==null||"".equals(user.getName())) {
			responseDto.setMessage("illegal name");
			errorInput = true;
		}else if(user.getOperation()==null||"".equals(user.getOperation())){
			responseDto.setMessage("illegal opreation");
			errorInput = true;
		}
		if (errorInput) {
			return responseDto;
		}
		responseDto = mService.updateStatusByName(user.getName(), user.getOperation());
		//info M/C
		StatusDto statusDto = new StatusDto(user.getName(), user.getOperation().toLowerCase(), "change");
		client.post(PropertiesUtils.getProperties("merchant.post.status"), JsonUtil.toJsonString(statusDto));
		MerchantChangeDto merchantChangeDto = new MerchantChangeDto("change");
		client.post(PropertiesUtils.getProperties("customer.post.status"), JsonUtil.toJsonString(merchantChangeDto));
		
		
		return responseDto;
	}
	
}
