package controller.merchant;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import pojo.AdSpace;
import pojo.Merchant;
import service.AdSpaceService;
import service.MerchantService;
import util.GetMerchantStatusUtil;
import dto.AdSpaceToMDto;
import dto.StatusDto;

@Controller
public class MerchantRespController {

	@Resource(name="merchantService")
	private MerchantService mService;
	@Resource(name="adSpaceService")
	private AdSpaceService adService;
	
	@ResponseBody
	@RequestMapping(value="/api/merchant/{name}", method=RequestMethod.GET)
	public StatusDto mStatusToM(@PathVariable String name){
		Merchant merchant = mService.getByName(name).get(0);
		String status = null;
		StatusDto statusDto = new StatusDto();
		if(merchant != null){
			status = merchant.getStatus();
			statusDto.setMessage(merchant.getRejectReason());
		}
		String merchantStatus = GetMerchantStatusUtil.getStatus(status);
		statusDto.setStatus(merchantStatus);
		return statusDto;
	}
	
	@ResponseBody
	@RequestMapping(value="/api/adspace/{id}", method=RequestMethod.GET)
	public AdSpaceToMDto aStatusToM(@PathVariable String id){
		return adService.getById(id);
	}
	
}
