package controller;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import component.sender.AdvertiseStatusSender;
import dto.AdSpaceDto;
import dto.AdSpaceToMDto;
import dto.ResponseDto;
import pojo.AdSpace;
import service.AdSpaceService;
import util.JsonUtil;

@Controller
public class AdSpaceController {

	@Resource(name="adSpaceService")
	private AdSpaceService aService;
	@Resource(name="advertiseStatusSender")
	private AdvertiseStatusSender adSender;
	
	/*
	 * replace with jms
	 */
	@ResponseBody
	@RequestMapping(value="/adspace", method=RequestMethod.POST)
	public AdSpace receive(@RequestBody AdSpaceDto adSpace){
		return aService.add(adSpace);
	}
	
	@ResponseBody
	@RequestMapping(value="/adspace/{status}", method=RequestMethod.GET)
	public ResponseDto getByStatus(@PathVariable String status){
		ResponseDto rDto = aService.getByStatus(status);
		return rDto;
	}
	
	@ResponseBody
	@RequestMapping(value="/adspace", method=RequestMethod.PUT)
	public ResponseDto audit(@RequestBody AdSpaceDto msg){
		AdSpace aSpace = aService.updateStaus(msg);
		if(aSpace != null){
			AdSpaceToMDto aDto = new AdSpaceToMDto(aSpace.getReqId(), aSpace.getStatus());
			String msg2 = JsonUtil.toJsonString(aDto);
			adSender.send(msg2);
			
			ResponseDto responseDto = new ResponseDto(1, "success", aSpace);
			return responseDto;
		}
		return null;
	}
	
}
