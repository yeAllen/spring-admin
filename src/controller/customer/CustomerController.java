package controller.customer;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import pojo.AdSpace;
import pojo.Merchant;
import service.AdSpaceService;
import service.MerchantService;
import util.GetMerchantDtoListUtil;
import util.PropertiesUtils;
import data.VersionTable;
import dto.AdSpaceToCDto;
import dto.MerchantDto;
import dto.StatusVersionDto;

@Controller
public class CustomerController {

	@Resource(name="merchantService")
	private MerchantService mService;
	@Resource(name="adSpaceService")
	private AdSpaceService aService;
	@Resource(name="versionTable")
	private VersionTable versionTable;
	
	@ResponseBody
	@RequestMapping(value="/api/customer/list", method=RequestMethod.GET)
	public List<MerchantDto> getMerchantList(){
		List<Merchant> mList = mService.getByStatus(Merchant.WHITE_LIST);
		List<MerchantDto> mDtos = GetMerchantDtoListUtil.getMerchantDto(mList);
//		for(MerchantDto m:mDtos){
//			String path = PropertiesUtils.getProperties("image.get.path")+m.getAvatar();
//			m.setAvatar(path);
//		}
		return mDtos;
	}
	
	@ResponseBody
	@RequestMapping(value="/api/customer/verssion", method=RequestMethod.GET)
	public StatusVersionDto getStatus(){
	    StatusVersionDto sVersionDto = new StatusVersionDto();
	    sVersionDto.setVersion(versionTable.get(VersionTable.STATUS_CHANGE__VERSION));
	    return sVersionDto;
	}
	
	@CrossOrigin(origins = "*", maxAge = 3600)
	@ResponseBody
	@RequestMapping(value="/api/customer/ads", method=RequestMethod.GET)
	public List<AdSpaceToCDto> getAdSpace(){
		return aService.getPassInTime(AdSpace.PASS, new Date());
	}
	
}
