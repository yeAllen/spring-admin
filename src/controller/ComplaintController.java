package controller;

import java.util.List;

import javax.annotation.Resource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import dto.ComplaintDto;
import dto.ResponseDto;
import pojo.Complaint;
import service.ComplaintService;

@Controller
public class ComplaintController {
	
	protected Logger logger = LogManager.getLogger(this.getClass());
	@Resource(name="complaintService")
	private ComplaintService complaintService;

	/*
	 * replace with jms
	 */
	@ResponseBody
	@RequestMapping(value="/complaint", method=RequestMethod.POST)
	public Complaint receive(@RequestBody ComplaintDto complaint){
		try {
			Complaint complaint2 = complaintService.add(complaint);
			return complaint2;
		} catch (Exception e) {
			logger.error(e);
			return null;
		}
	}
	
	@ResponseBody
	@RequestMapping(value="/complaint/{status}", method=RequestMethod.GET)
	public ResponseDto getByStatus(@PathVariable String status){
		ResponseDto reDto = complaintService.getByStatus(status);
		return reDto;
//		return complaintService.getByStatus(status);
	}

	@ResponseBody
	@RequestMapping(value="/complaint", method=RequestMethod.PUT)
	public Complaint handleComplaint(@RequestBody ComplaintDto msg){
		try {
			return complaintService.updateStatus(msg);
		} catch (Exception e) {
			logger.error(e);
			return null;
		}
	}
	
}
