package controller;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import pojo.Admin;
import service.AdminService;

@Controller
@SessionAttributes("name")
public class AdminController {

	@Resource(name="adminService")
	private AdminService aService;
	
	@RequestMapping(value="/admin", method=RequestMethod.POST)
	public String login(Admin admin, Model model){
		Admin admin2 = aService.login(admin.getName(), admin.getPassword());
		if(admin2!=null){
			model.addAttribute("name", admin2.getName());
			return "redirect:/home.html";
		}else{
			model.addAttribute("errormsg", "用户名或密码错误");
			return "login";
		}
		
	}
}
