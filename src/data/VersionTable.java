package data;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class VersionTable {
	
	public final static String AUDIT_VERSION = "auditVersion"; 
	public final static String STATUS_CHANGE__VERSION = "statusVersion"; 
	
	private final static Logger logger = LogManager.getLogger(VersionTable.class);
	
	private Map<String, Long> versionMap;
	
	
	public VersionTable(){
		versionMap = new ConcurrentHashMap<String, Long>();
	}
	
	public void init() {
	  	long time = System.currentTimeMillis();
	  	versionMap.put(VersionTable.AUDIT_VERSION, time);
	  	versionMap.put(VersionTable.STATUS_CHANGE__VERSION, time);
	  	logger.info("versionTable build.......");
	}
	
	
	public void put(String name,Long version){
		versionMap.put(name, version);
	}
	
	public Long get(String name){
		return versionMap.get(name);
	}
	

}
