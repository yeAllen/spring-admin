package data;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

@Component(value="adSpaceTimestamp")
public class AdSpaceTimeStamp {
	
	private final static Logger logger = LogManager.getLogger(AdSpaceTimeStamp.class);
	
	public static Map<String, String> timeStampMap;
	
	
	public AdSpaceTimeStamp(){
		timeStampMap = new ConcurrentHashMap<String, String>();
		init();
	}
	
	public static void init() {
	  	timeStampMap.put("00:00:00", "08:00:00");
	  	timeStampMap.put("08:00:00", "16:00:00");
	  	timeStampMap.put("16:00:00", "23:59:59");
	  	logger.info("adSpaceTimeStamp build.......");
	}
}
