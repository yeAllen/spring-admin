package util;

import java.util.ArrayList;
import java.util.List;

import pojo.Merchant;
import dto.MerchantDto;

public class GetMerchantDtoListUtil {

	public static List<MerchantDto> getMerchantDto(List<Merchant> mList){
		List<MerchantDto> mDtos = new ArrayList<>();
		for(Merchant m:mList){
			MerchantDto mDto = new MerchantDto(m.getName(), m.getAvatar(), m.getAddress());
			mDtos.add(mDto);
		}
		return mDtos;
	}
}
