package util;

import java.util.ArrayList;
import java.util.List;

import pojo.Merchant;

public class PackAsMerchantUtil {

	public static List<Merchant> pack(List<Merchant> mList){
		List<Merchant> merchants = new ArrayList<>();
		if(mList.size()>0){
			for(Merchant m:mList){
				Merchant merchant=new Merchant(null, m.getName(), m.getIdCard(), m.getAvatar(), 
						m.getAddress(), m.getStatus());
				merchants.add(merchant);
			}
		}
		return merchants;
	}
}
