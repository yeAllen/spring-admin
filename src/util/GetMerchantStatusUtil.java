package util;

import dto.StatusDto;
import pojo.Merchant;

public class GetMerchantStatusUtil {

	public static String getStatus(String status){
		if(Merchant.BLACK_LIST.equals(status))
			return StatusDto.BLACK_LIST;
		if(Merchant.WHITE_LIST.equals(status))
			return StatusDto.WHITE_LIST;
		if(Merchant.REJECT.equals(status))
			return StatusDto.REJECT;
		if(Merchant.WAIT_AUDIT.equals(status))
			return StatusDto.WAIT_AUDIT;
		return null;
	}
}
