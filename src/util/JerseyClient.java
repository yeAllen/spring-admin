package util;

import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.json.JSONConfiguration;

public class JerseyClient {

	 private ClientConfig clientConfig;

	    public JerseyClient(){
	        clientConfig = new DefaultClientConfig();
	        clientConfig.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING, Boolean.TRUE);
//	        clientConfig.getClasses().add(MultiPartWriter.class);
	    }

	    public String get(String url){
	        Client writerClient = Client.create(clientConfig);
	        writerClient.setConnectTimeout(3000);
	        writerClient.setReadTimeout(3000);
	        WebResource resource = writerClient.resource(url);
	        try{
	            String response =resource.accept(MediaType.APPLICATION_JSON).type(MediaType.MULTIPART_FORM_DATA).get(String.class);
	            return response;
	        }finally {
	            writerClient.destroy();
	        }

	    }

	    public String post(String url,String json){
	        Client writerClient = Client.create(clientConfig);
	        writerClient.setConnectTimeout(3000);
	        writerClient.setReadTimeout(3000);
	        WebResource resource = writerClient.resource(url);
	        try{
	            String response =resource.accept(MediaType.APPLICATION_JSON).type(MediaType.APPLICATION_JSON).post(String.class,json);
	            return response;
	        }finally {
	            writerClient.destroy();
	        }

	    }
}
