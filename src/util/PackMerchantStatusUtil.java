package util;

import java.util.HashMap;
import java.util.Map;

import pojo.Merchant;
import dto.StatusDto;

public class PackMerchantStatusUtil {

	public static Map<String, String> getStatus(){
		Map<String, String> status = new HashMap<String, String>();
		status.put(Merchant.WAIT_AUDIT, StatusDto.WAIT_AUDIT);
		status.put(Merchant.REJECT, StatusDto.REJECT);
		status.put(Merchant.BLACK_LIST, StatusDto.BLACK_LIST);
		status.put(Merchant.WHITE_LIST, StatusDto.WHITE_LIST);
		return status;
	}
}
