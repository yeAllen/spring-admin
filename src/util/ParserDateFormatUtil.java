package util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ParserDateFormatUtil {

	protected Logger logger = LogManager.getLogger(this.getClass());
	public Date parser(Date date, String format){
		SimpleDateFormat sFormat = new SimpleDateFormat(format);
		try {
			Date time = sFormat.parse(sFormat.format(date));
			return time;
		} catch (ParseException e) {
			logger.error(e);
		}
		return null;
	}
}
