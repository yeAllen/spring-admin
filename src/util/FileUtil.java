package util;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class FileUtil {
	
	public static String upload(InputStream in, String upLoadPath){
        String base64String = null;
        try {
            byte[] buffer = new byte[512];
            int len = 0;
            FileOutputStream out = new FileOutputStream(upLoadPath);
            while ((len = in.read(buffer)) != -1) {
                out.write(buffer, 0, len);
            }
            base64String = Base64Util.GetImageStr(upLoadPath);
            in.close();
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return base64String;
    }

}
