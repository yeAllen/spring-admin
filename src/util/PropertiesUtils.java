package util;

import java.io.IOException;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class PropertiesUtils {
	
	private final static Logger logger = LogManager.getLogger(PropertiesUtils.class);
	
	private static Properties properties;
//	private final static String path = "admin.properties";
	private final static String path = "imgServer.properties";
	
	static{
		properties = new Properties();
		try {
			logger.info(PropertiesUtils.class.getClassLoader().getResource(path));
			properties.load(PropertiesUtils.class.getClassLoader().getResourceAsStream(path));
		} catch (IOException e) {
			logger.error(e);
		}
	}
	
	public static String getProperties(String name){
		String str = null;
		if (properties.containsKey(name)) {
			return properties.getProperty(name);
		}
		return str;
	}

}
