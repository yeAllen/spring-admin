package util;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

public class MyJsonSeriallizer extends JsonSerializer<String>{

	@Override
	public void serialize(String value, JsonGenerator gen,
			SerializerProvider serializers) throws IOException,
			JsonProcessingException {
		String path = PropertiesUtils.getProperties("image.get.path");
		gen.writeString(path+value);
	}

}
