package vo;

public class MerchantVO {
	
	private String name;
	private String idCard;
	private String avatar;
	private String address;
	private String status;
	
	public MerchantVO(){}
	public MerchantVO(String name, String idCard, String avatar,
			String address, String status) {
		super();
		this.name = name;
		this.idCard = idCard;
		this.avatar = avatar;
		this.address = address;
		this.status = status;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getIdCard() {
		return idCard;
	}
	public void setIdCard(String idCard) {
		this.idCard = idCard;
	}
	public String getAvatar() {
		return avatar;
	}
	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	@Override
	public String toString() {
		return "MerchantVO [name=" + name + ", idCard="
				+ idCard + ", avatar=" + avatar + ", address="
				+ address + ", status=" + status + "]";
	}
	
}
