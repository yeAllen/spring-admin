package pojo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;

@Entity
@GenericGenerator(strategy="uuid",name="merchant")
public class Merchant implements Serializable{
	
	private static final long serialVersionUID = 1L;
	public final static String WAIT_AUDIT = "audit";
	public final static String REJECT = "reject";
	public final static String BLACK_LIST = "black_list";
	public final static String WHITE_LIST = "white_list";
	
	@Id
	@GeneratedValue(generator="merchant")
	private String id;
	@Column(nullable=false,length=40,updatable=false,unique=true)
	private String name;
//	@Column(columnDefinition="BLOB")
	private String idCard;
	private String avatar;
	@Column(length=100)
	private String address;
	@Column(length=20)
	private String status;
	private String rejectReason;
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateTime; 
//	@OneToMany(mappedBy="merchant",fetch=FetchType.EAGER)
	@OneToMany(mappedBy="merchant")
	private List<Complaint> complaints = new ArrayList<>();
//	@OneToMany(mappedBy="merchant",fetch=FetchType.EAGER)
	@OneToMany(mappedBy="merchant")
	private List<AdSpace> adSpaces = new ArrayList<>();
	
	public Merchant() {
		super();
	}
	public Merchant(String id, String name, String status) {
		super();
		this.id = id;
		this.name = name;
		this.status = status;
	}
	public Merchant(String id, String name, String address, String status) {
		super();
		this.id = id;
		this.name = name;
		this.address = address;
		this.status = status;
	}
	public Merchant(String id, String name, String idCard,
			String avatar, String address, String status) {
		super();
		this.id = id;
		this.name = name;
		this.idCard = idCard;
		this.avatar = avatar;
		this.address = address;
		this.status = status;
	}
	public Merchant(String id, String name, String idCard,
			String avatar, String address, String status, String rejectReason) {
		super();
		this.id = id;
		this.name = name;
		this.idCard = idCard;
		this.avatar = avatar;
		this.address = address;
		this.status = status;
		this.rejectReason = rejectReason;
	}
	public Merchant(String name, String idCard, String avatar, String address,
			String status, String rejectReason, Date updateTime) {
		super();
		this.name = name;
		this.idCard = idCard;
		this.avatar = avatar;
		this.address = address;
		this.status = status;
		this.rejectReason = rejectReason;
		this.updateTime = updateTime;
	}
	public Merchant(String id, String name, String idCard,
			String avatar, String address, String status, String rejectReason,
			Date updateTime) {
		super();
		this.id = id;
		this.name = name;
		this.idCard = idCard;
		this.avatar = avatar;
		this.address = address;
		this.status = status;
		this.rejectReason = rejectReason;
		this.updateTime = updateTime;
	}

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAvatar() {
		return avatar;
	}
	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getRejectReason() {
		return rejectReason;
	}
	public void setRejectReason(String rejectReason) {
		this.rejectReason = rejectReason;
	}
	public Date getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	public String getIdCard() {
		return idCard;
	}
	public void setIdCard(String idCard) {
		this.idCard = idCard;
	}
	public List<Complaint> getComplaints() {
		return complaints;
	}
	public void setComplaints(List<Complaint> complaints) {
		this.complaints = complaints;
	}
	
	@Override
	public String toString() {
		return "Merchant [id=" + id + ", name=" + name + ", address=" + address
				+ ", status=" + status + ", rejectReason=" + rejectReason
				+ ", updateTime=" + updateTime + "]";
	}
}
