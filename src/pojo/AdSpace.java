package pojo;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import java.util.TimeZone;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@GenericGenerator(strategy="uuid", name="adspace")
public class AdSpace implements Serializable{

	private static final long serialVersionUID = 1L;
	public static final String TIMEOUT = "timeout";
	public static final String PASS = "pass";
	public static final String UNPASS = "unpass";
	public static final String DEFAULT = "audit";
	public static final String SUBSTITUTION = "substitution";
	public static final int timeBlock = 2;
	public static final Timestamp timestamp = new Timestamp(System.currentTimeMillis()/(1000*3600*24)*(1000*3600*24)-TimeZone.getDefault().getRawOffset());
	
	@Id
	@GeneratedValue(generator="adspace")
	private String id;
	@Column(nullable=false)
	private String reqId;
	private String imgPath;
	@Column(nullable=false,updatable=false)
	private double adPrice;
	private String foodId;
	@Temporal(TemporalType.TIMESTAMP)
	private Date startTime;
	@Temporal(TemporalType.TIMESTAMP)
	private Date endTime;
	@Column(length=20,nullable=false)
//	@Enumerated(EnumType.STRING)
	private String status;
	@ManyToOne
	@JoinColumn(name="m_name")
	@JsonBackReference
	private Merchant merchant;
	private String mid;
	
	public AdSpace(){}
	public AdSpace(String id, double adPrice) {
		super();
		this.id = id;
		this.adPrice = adPrice;
	}
	public AdSpace(String id, double adPrice, String status) {
		super();
		this.id = id;
		this.adPrice = adPrice;
		this.status = status;
	}
	public AdSpace(String id, String reqId, String imgPath, double adPrice,
			String foodId, Date startTime, Date endTime, String status,
			Merchant merchant) {
		super();
		this.id = id;
		this.reqId = reqId;
		this.imgPath = imgPath;
		this.adPrice = adPrice;
		this.foodId = foodId;
		this.startTime = startTime;
		this.endTime = endTime;
		this.status = status;
		this.merchant = merchant;
	}
	public AdSpace(String id, String reqId, String imgPath, double adPrice,
			String foodId, Date startTime, Date endTime, String status,
			Merchant merchant, String mid) {
		super();
		this.id = id;
		this.reqId = reqId;
		this.imgPath = imgPath;
		this.adPrice = adPrice;
		this.foodId = foodId;
		this.startTime = startTime;
		this.endTime = endTime;
		this.status = status;
		this.merchant = merchant;
		this.mid = mid;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getImgPath() {
		return imgPath;
	}
	public void setImgPath(String imgPath) {
		this.imgPath = imgPath;
	}
	public double getAdPrice() {
		return adPrice;
	}
	public void setAdPrice(double adPrice) {
		this.adPrice = adPrice;
	}
	public String getFoodId() {
		return foodId;
	}
	public void setFoodId(String foodId) {
		this.foodId = foodId;
	}
	public Date getStartTime() {
		return startTime;
	}
	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}
	public Date getEndTime() {
		return endTime;
	}
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}
	public int getTimeBlock() {
		return timeBlock;
	}
	public Timestamp getTimestamp() {
		return timestamp;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Merchant getMerchant() {
		return merchant;
	}
	public void setMerchant(Merchant merchant) {
		this.merchant = merchant;
	}
	public String getReqId() {
		return reqId;
	}
	public void setReqId(String reqId) {
		this.reqId = reqId;
	}
	public String getMid() {
		return mid;
	}
	public void setMid(String mid) {
		this.mid = mid;
	}
	@Override
	public String toString() {
		return "AdSpace [id=" + id + ", imgPath=" + imgPath + ", adPrice="
				+ adPrice + ", foodId=" + foodId + ", startTime=" + startTime
				+ ", endTime=" + endTime + ", status=" + status + ", merchant="
				+ merchant + "]";
	}
	
}
