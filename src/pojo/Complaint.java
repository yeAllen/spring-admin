package pojo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@GenericGenerator(strategy="uuid",name="complaint")
public class Complaint implements Serializable{

	private static final long serialVersionUID = 1L;
	public static final String UNHANDLE = "unhandle";
	public static final String HANDLE = "handle";
	public static final String DANGER = "danger";
	@Id
	@GeneratedValue(generator="complaint")
	private String id;
	@Column(nullable=false,length=40,updatable=false)
	private String cname;
	@Column(nullable=false,updatable=false)
	private String complaintContext;
	private String complaintStatus;
	@Temporal(TemporalType.TIMESTAMP)
	@Column(updatable=false)
	private Date complaintDate;
	@ManyToOne
	@JoinColumn(name="m_name")
	@JsonBackReference
	private Merchant merchant;
	
	public Complaint(){}
	
	public Complaint(String id, String cname, String complaintContext,
			String complaintStatus) {
		super();
		this.id = id;
		this.cname = cname;
		this.complaintContext = complaintContext;
		this.complaintStatus = complaintStatus;
	}

	public Complaint(String id, String cname, String complaintContext,
			String complaintStatus, Date complaintDate, Merchant merchant) {
		super();
		this.id = id;
		this.cname = cname;
		this.complaintContext = complaintContext;
		this.complaintStatus = complaintStatus;
		this.complaintDate = complaintDate;
		this.merchant = merchant;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getCname() {
		return cname;
	}
	public void setCname(String cname) {
		this.cname = cname;
	}
	public String getComplaintContext() {
		return complaintContext;
	}
	public void setComplaintContext(String complaintContext) {
		this.complaintContext = complaintContext;
	}
	public String getComplaintStatus() {
		return complaintStatus;
	}
	public void setComplaintStatus(String complaintStatus) {
		this.complaintStatus = complaintStatus;
	}
	public Date getComplaintDate() {
		return complaintDate;
	}
	public void setComplaintDate(Date complaintDate) {
		this.complaintDate = complaintDate;
	}
	public Merchant getMerchant() {
		return merchant;
	}
	public void setMerchant(Merchant merchant) {
		this.merchant = merchant;
	}
	@Override
	public String toString() {
		return "Complaint [id=" + id + ", cname=" + cname
				+ ", complaintContext=" + complaintContext
				+ ", complaintStatus=" + complaintStatus + ", complaintDate="
				+ complaintDate + ", merchant=" + merchant + "]";
	}
	
	
}
