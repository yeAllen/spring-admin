package pojo;

import com.fasterxml.jackson.annotation.JsonValue;

public enum AdSpaceStatusEnum {

	PASS("pass"),
	UNPASS("unpass"),
	DEFAULT("audit");
	
	private String status;

	private AdSpaceStatusEnum(String status) {
		this.status = status;
	}
	
	@JsonValue
	public String getStatus() {
		return status;
	}

}
