package component.action;

import javax.annotation.Resource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import service.ComplaintService;
import component.Action;
import dto.ComplaintDto;

@Component(value="complaintAction")
public class ComplaintAction implements Action<ComplaintDto>{

	private final static Logger logger = LogManager.getLogger(ComplaintAction.class);
	@Resource(name="complaintService")
	private ComplaintService complaintService;
	
	@Override
	public void action(ComplaintDto complaintDto) {
		try {
			complaintService.add(complaintDto);
		} catch (Exception e) {
			logger.error("complaint insert error "+e);
		}
	}

	
}
