package component.action;

import javax.annotation.Resource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import service.AdSpaceService;
import component.Action;
import component.listener.ComplaintListener;
import dto.AdSpaceDto;

@Component(value="adSpaceAction")
public class AdSpaceAction implements Action<AdSpaceDto>{

	private final static Logger logger = LogManager.getLogger(ComplaintListener.class);
	@Resource(name="adSpaceService")
	private AdSpaceService aService;
	
	@Override
	public void action(AdSpaceDto object) {
		try {
			logger.info("from m......"+object.toString());
			aService.add(object);
		} catch (Exception e) {
			logger.error("adspace insert error "+e);
		}
	}
}
