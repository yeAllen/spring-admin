package component.listener;



import javax.annotation.Resource;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import util.JsonUtil;
import component.action.AdSpaceAction;
import component.action.ComplaintAction;
import dto.AdSpaceDto;

public class AdvertisementListener implements MessageListener {

	private final static Logger logger = LogManager.getLogger(ComplaintListener.class);
	@Resource(name="adSpaceAction")
	private AdSpaceAction adSpaceAction;
	
    public void onMessage(Message message) {
        if (message instanceof TextMessage) {
            try {
            	TextMessage textMessage = (TextMessage)message;
                logger.info(textMessage.getText());
                AdSpaceDto adSpaceDto = (AdSpaceDto)JsonUtil.toJsonObject(((TextMessage) message).getText(), AdSpaceDto.class);
                adSpaceAction.action(adSpaceDto);
                textMessage.acknowledge();
            }
            catch (JMSException ex) {
            	logger.error(ex);
            }
        }
        else {
        	logger.info("Message must be of type TextMessage:"+message);
        }
    }
}
