package component.listener;

import javax.annotation.Resource;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import component.Action;
import dto.Protocol;
import util.JsonUtil;

public class MerchantListener implements MessageListener{
	
	private final static Logger logger = LogManager.getLogger(MerchantListener.class);
	
	@Resource(name="messageAction")
	private Action messageAction;
	
	 public void onMessage(Message message) {
	        if (message instanceof TextMessage) {
	        	TextMessage textMessage = (TextMessage) message;
				try {
					logger.info(textMessage.getText());					
					Protocol protocol = (Protocol) JsonUtil.toJsonObject(textMessage.getText(), Protocol.class);
					messageAction.action(protocol);
					textMessage.acknowledge();
				} catch (JMSException e) {
					logger.error(e);
				}
	        }
	        else {
	           logger.info("not a text message:"+message);
	        }
	    }

}
