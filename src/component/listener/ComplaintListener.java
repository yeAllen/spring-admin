package component.listener;

import javax.annotation.Resource;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import component.action.ComplaintAction;

import util.JsonUtil;
import dto.ComplaintDto;

public class ComplaintListener implements MessageListener {

	private final static Logger logger = LogManager.getLogger(ComplaintListener.class);
	@Resource(name="complaintAction")
	private ComplaintAction complaintAction;
	
    public void onMessage(Message message) {
        if (message instanceof TextMessage) {
            try {
                TextMessage textMessage = (TextMessage)message;
                logger.info(textMessage.getText());
                ComplaintDto complaintDto = (ComplaintDto)JsonUtil.toJsonObject(textMessage.getText(), ComplaintDto.class);
                complaintAction.action(complaintDto);
                textMessage.acknowledge();
            }
            catch (JMSException ex) {
            	logger.error(ex);
            }
        }
        else {
            logger.info("Message must be of type TextMessage:"+message);
        }
    }
}
