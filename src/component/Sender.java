package component;



public interface Sender {

    public int send(String msg);
}
