package component;

public interface Action<T> {
	
	public void action(T object);

}
