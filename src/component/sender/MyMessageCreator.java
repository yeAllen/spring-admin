package component.sender;


import org.springframework.jms.core.MessageCreator;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;

class MyMessageCreator implements MessageCreator {

    private String msg;
    MyMessageCreator(String msg){
        this.msg = msg;
    }

    public Message createMessage(Session session) throws JMSException {
        return session.createTextMessage(msg);
    }
}