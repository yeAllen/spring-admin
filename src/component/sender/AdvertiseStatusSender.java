package component.sender;



import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

import component.Sender;

import javax.annotation.Resource;
import javax.jms.Queue;

@Component("advertiseStatusSender")
public class AdvertiseStatusSender implements Sender{

    @Resource(name = "jmsTemplate")
    private JmsTemplate jmsTemplate;
    @Resource(name = "advertiseStatusQueue")
    private Queue queue;


    public int send(String msg) {
        this.jmsTemplate.send(this.queue, new MyMessageCreator(msg));
        return 1;
    }

}
