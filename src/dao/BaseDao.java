package dao;

import java.util.List;

public interface BaseDao<T> {
	public T add(T t);
	public T delete(String id);
	public T update(T t);
	public T load(String id);
	public List<T> findAll();

}
