package dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import pojo.Complaint;
import dao.ComplaintDao;

@Repository(value="complaintDao")
public class ComplaintDaoImpl implements ComplaintDao{

	@PersistenceContext(name="un")
	private EntityManager manager;
	
	@Override
	public Complaint add(Complaint t) {
		manager.persist(t);
		return t;
	}

	@Override
	public Complaint delete(String id) {
		return null;
	}

	@Override
	public Complaint update(Complaint t) {
		Complaint complaint = manager.find(Complaint.class, t.getId());
		if(complaint!=null){
			complaint.setCname(t.getCname());
			complaint.setComplaintContext(t.getComplaintContext());
			complaint.setComplaintStatus(t.getComplaintStatus());
			complaint.setComplaintDate(t.getComplaintDate());
			complaint.setMerchant(t.getMerchant());
			manager.merge(complaint);
			return complaint;
		}
		return null;
	}

	@Override
	public Complaint load(String id) {
		return null;
	}

	@Override
	public List<Complaint> findAll() {
		String jpql = "SELECT c FROM Complaint c";
		List<Complaint> complaints = manager.createQuery(jpql).getResultList();
		return complaints;
	}

	@Override
	public List<Complaint> findByStatus(String status) {
		String jpql = "SELECT c FROM Complaint c WHERE c.complaintStatus=:status";
		List<Complaint> complaints = manager.createQuery(jpql)
				.setParameter("status", status)
				.getResultList();
		return complaints;
	}

}
