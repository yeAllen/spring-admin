package dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import pojo.Admin;
import dao.AdminDao;

@Repository(value="adminDao")
public class AdminDaoImpl extends BaseDaoImpl<Admin> implements AdminDao {

	@PersistenceContext(name="un")
	private EntityManager manager;
	
	@Override
	public Admin add(Admin a) {
		manager.persist(a);
		return a;
	}

	@Override
	public Admin delete(String id) {
		return null;
	}

	@Override
	public Admin update(Admin t) {
		return null;
	}

	@Override
	public Admin load(String id) {
		return null;
	}

	@Override
	public List<Admin> findAll() {
		return null;
	}

	@Override
	public Admin findByName(String name, String password) {
		String jpql = "SELECT a FROM Admin a WHERE a.name=:name AND a.password=:password";
		List<Admin> admins = manager.createQuery(jpql)
				.setParameter("name", name)
				.setParameter("password", password)
				.getResultList();
		if(admins.size() > 0)
			return admins.get(0);
		return null;
	}
	
}
