package dao;

import pojo.Admin;

public interface AdminDao extends BaseDao<Admin> {
	public Admin findByName(String name, String password);
}
