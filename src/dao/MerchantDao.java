package dao;

import java.util.List;

import pojo.Merchant;

public interface MerchantDao extends BaseDao<Merchant> {
	
	public Merchant updateStatusByName(String name,String status);
	
	public List<Merchant> getByName(String name) ;
	
	public List<Merchant> findByStatus(String status);
	
	public Merchant updateByName(Merchant merchant);

}
