package dao;

import java.util.List;

import pojo.Complaint;

public interface ComplaintDao extends BaseDao<Complaint>{

	public List<Complaint> findByStatus(String status);
}
