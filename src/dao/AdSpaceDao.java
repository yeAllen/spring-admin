package dao;

import java.util.Date;
import java.util.List;

import dto.AdSpaceDto;
import pojo.AdSpace;
import pojo.AdSpaceStatusEnum;

public interface AdSpaceDao extends BaseDao<AdSpace>{

	public AdSpaceDto addAds(AdSpaceDto adSpace);
	public List<AdSpace> getByStatus(String status);
	//to judge if the new one can pass
	public List<AdSpace> getByTimeStamp(Date startTime, String status);
	//get ads in one time
	public List<AdSpace> getAdsInOneTime(Date time, String status);
}
