package test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import util.MyJsonSeriallizer;
import util.PropertiesUtils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import data.AdSpaceTimeStamp;

public class JsonDemo {

	@JsonSerialize(using=MyJsonSeriallizer.class)
	String name;
	String password;

	public JsonDemo() {
	}

	public JsonDemo(String name, String password) {
	this.name = name;
	this.password = password;
	}

	public String getName() {
	return name;
	}

	public void setName(String name) {
	this.name = name;
	}

	public String getPassword() {
	return password;
	}

	public void setPassword(String password) {
	this.password = password;
	}

	public static void main(String[] args) throws JsonProcessingException {
//		JsonDemo j = new JsonDemo("marb", "afsdfsd");
//		ObjectMapper objectMapper = new ObjectMapper();
//		String json = objectMapper.writeValueAsString(j);
//		System.out.println(json);
		
//		SimpleDateFormat dFormat = new SimpleDateFormat("HH:mm:ss");
//		String re = dFormat.format(new Date());
//		System.out.println(re);
		
//		String sum = PropertiesUtils.getProperties("adspace.timestamp.sum");
//		System.out.println(Integer.valueOf(sum));
		
//		AdSpaceTimeStamp aStamp = new AdSpaceTimeStamp();
//		String endString = aStamp.timeStampMap.get("08:00:00");
//		System.out.println(endString+"...end");
		
		
//		String starttime = "2017-8-15 16:00:00";
//		SimpleDateFormat dFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//		try {
//			Date sDate = dFormat.parse(starttime);
//			System.out.println((new Date().getTime()-sDate.getTime())>0);
//		} catch (ParseException e) {
//			e.printStackTrace();
//		}
				
	}

}
