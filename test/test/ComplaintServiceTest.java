package test;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import dto.ComplaintDto;
import dto.ResponseDto;
import pojo.Admin;
import pojo.Complaint;
import service.AdminService;
import service.ComplaintService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath:applicationContext.xml"})
public class ComplaintServiceTest {

	@PersistenceContext(name="un")
	private EntityManager manager;
	@Resource(name="complaintService")
	private ComplaintService cService;
	
	
	@Test
	public void testAdd(){
		ComplaintDto complaint = new ComplaintDto("lionel1", "难吃", null,"merchant4");
		Assert.assertNotNull(cService.add(complaint));
		ComplaintDto complaint1 = new ComplaintDto("lionel2", "不好吃", null,"merchant4");
		Assert.assertNotNull(cService.add(complaint1));
		ComplaintDto complaint2 = new ComplaintDto("lionel2", "不好吃", null,"merchant4");
		Assert.assertNotNull(cService.add(complaint2));
	}
	
	@Test
	public void testFindAll(){
		List<Complaint> complaints = cService.getAll();
		Assert.assertNotNull(complaints);
		System.out.println(complaints.get(0).toString());
	}
	
	@Test
	public void testGetByStatus(){
//		List<Complaint> complaints = cService.getByStatus(Complaint.UNHANDLE);
		ResponseDto complaints = cService.getByStatus(Complaint.HANDLE);
		Assert.assertNotNull(complaints);

	}
	
	@Test
	public void testUpdate(){
//		ComplaintDto complaint = new ComplaintDto("8a5e9d1f5dd50172015dd50175880000", "harry", "yibanban", Complaint.HANDLE);
//		Complaint complaint2 = cService.updateStatus(complaint);
//		System.out.println("update..."+complaint2);
//		Assert.assertNotNull(complaint2);
	}
	
}
