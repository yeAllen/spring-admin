package test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import oracle.net.aso.s;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import dto.ResponseDto;
import dto.UpdateMerchatStatusDto;
import pojo.Admin;
import pojo.Merchant;
import service.AdminService;
import service.MerchantService;
import vo.MerchantVO;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath:applicationContext.xml"})
public class MerchantServiceTest {

	@PersistenceContext(name="un")
	private EntityManager manager;
	@Resource(name="merchantService")
	private MerchantService mService;
	
	@Test
	public void testAdd(){
		Merchant merchant = new Merchant(null, "merchant1",Merchant.WAIT_AUDIT);
		mService.add(merchant);
		Merchant merchant2 = new Merchant(null, "merchant2",Merchant.WAIT_AUDIT);
		mService.add(merchant2);
		Merchant merchant3 = new Merchant(null, "merchant3",Merchant.WAIT_AUDIT);
		mService.add(merchant3);
		
		Merchant merchant4 = new Merchant(null, "merchant4",Merchant.WHITE_LIST);
		mService.add(merchant4);
		Merchant merchant5 = new Merchant(null, "merchant5",Merchant.WHITE_LIST);
		mService.add(merchant5);
		Merchant merchant6 = new Merchant(null, "merchant6",Merchant.WHITE_LIST);
		mService.add(merchant6);

	}
	
	@Test
	public void testGetAll(){
		List<MerchantVO> merchants = mService.getAll();
		for(MerchantVO m:merchants){
			System.out.println(m.toString());
		}
	}
	
	@Test
	public void testupdateStatusByName(){
		ResponseDto rDto = mService.updateStatusByName("tommy", Merchant.BLACK_LIST);
		System.out.println("updateStatusByName..."+rDto.getMessage());
		Assert.assertEquals("1", rDto.getCode());
	}
	
	@Test
	public void testgetByStatus(){
		List<Merchant> merchants = mService.getByStatus(Merchant.WAIT_AUDIT);
		System.out.println("getbystatus..."+merchants.get(0).toString());
		Assert.assertNotNull(merchants);
	}
	
	@Test
	public void testgetByName(){
		Merchant merchant = mService.getByName("merchant5").get(0);
		System.out.println("getByName..."+merchant.toString());
		Assert.assertNotNull(merchant);
	}
	
	@Test
	public void testupdateByName(){
		Merchant merchant = new Merchant(null, "merchant5","香港",Merchant.WAIT_AUDIT);
		Assert.assertNotNull(mService.updateByName(merchant));
		Merchant merchant2 = new Merchant(null, "merchant15","香港",Merchant.WAIT_AUDIT);
		Assert.assertNull(mService.updateByName(merchant2));
	}
	
	@Test
	public void testaudit() throws ParseException{
//		Merchant merchant = new Merchant(null, "merchant8",Merchant.REJECT);
//		String status = mService.audit(merchant);
//		System.out.println("audit..."+status);
//		Assert.assertEquals("illegal change", status);
		
//		UpdateMerchatStatusDto merchant = new Merchant(null, "merchant1",Merchant.WHITE_LIST);
//		String status = mService.audit(merchant);
//		System.out.println("audit..."+status);
//		Assert.assertEquals("success", status);
	}
	
}
