package test;

import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import pojo.Admin;
import service.AdminService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath:applicationContext.xml"})
public class AdminServiceTest {

	@PersistenceContext(name="un")
	private EntityManager manager;
	@Resource(name="adminService")
	private AdminService aService;
	
	@Test
	public void testLogin(){
		Assert.assertNotNull(aService.login("admin","admin"));
	}
	
	@Test
	public void testRegist(){
		Admin admin = new Admin(null, "admin", "admin");
		aService.regist(admin);
	}
	
}
