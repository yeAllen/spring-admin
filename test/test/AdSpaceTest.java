package test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import dto.AdSpaceDto;
import dto.AdSpaceToCDto;
import dto.ResponseDto;
import pojo.AdSpace;
import pojo.AdSpaceStatusEnum;
import service.AdSpaceService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath:applicationContext.xml"})
public class AdSpaceTest {

	@PersistenceContext(name="un")
	private EntityManager manager;
	@Resource(name="adSpaceService")
	private AdSpaceService aService;
	
	@Test
	public void testAdd() throws ParseException{
//		AdSpaceDto aSpace = new AdSpaceDto(null, 90000.99);
//		Assert.assertNotNull(aService.add(aSpace));
		
		String starttime = "2017-8-17 16:00:00";
		String endtime = "2017-8-17 23:59:59";
		SimpleDateFormat dFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date sDate = dFormat.parse(starttime);
		Date eDate = dFormat.parse(endtime);
		AdSpaceDto aDto = new AdSpaceDto("8a5e9d1f5de50e3e015de50e434d1001", "merchant/avatar/3728e216-55a6-466f-8891-687e5ad26206.jpg", 186.99, "6a5e9d1f5de50e3e015de50e434d0001", sDate, eDate, "merchant5", "8a5e9d1f5de9a2ba015de9a2bff80004");
		Assert.assertNotNull(aService.add(aDto));
	}
	
	@Test
	public void init(){
		
		AdSpaceDto aDto = new AdSpaceDto(UUID.randomUUID().toString(), "defaultImg/timg.jpg", 0, null, null, null, null, null, null,null);
		AdSpaceDto aDto1 = new AdSpaceDto(UUID.randomUUID().toString(), "defaultImg/timg (1).jpg", 0, null, null, null, null, null, null,null);
		AdSpaceDto aDto2 = new AdSpaceDto(UUID.randomUUID().toString(), "defaultImg/timg (2).jpg", 0, null, null, null, null, null, null,null);
		AdSpaceDto aDto3 = new AdSpaceDto(UUID.randomUUID().toString(), "defaultImg/timg (4).jpg", 0, null, null, null, null, null, null,null);
		AdSpaceDto aDto4 = new AdSpaceDto(UUID.randomUUID().toString(), "defaultImg/timg (5).jpg", 0, null, null, null, null, null, null,null);
		AdSpaceDto aDto5 = new AdSpaceDto(UUID.randomUUID().toString(), "defaultImg/timg (6).jpg", 0, null, null, null, null, null, null,null);
		Assert.assertNotNull(aService.add(aDto));
		Assert.assertNotNull(aService.add(aDto1));
		Assert.assertNotNull(aService.add(aDto2));
		Assert.assertNotNull(aService.add(aDto3));
		Assert.assertNotNull(aService.add(aDto4));
		Assert.assertNotNull(aService.add(aDto5));
	}
	
	@Test
	public void testGetByStatus(){
		ResponseDto adSpaces = aService.getByStatus("audit");
//		for(AdSpace a:adSpaces.getData()){
//			System.out.println("get..."+a.toString());
//		}
		Assert.assertNotNull(adSpaces);
	}
	
	@Test
	public void testUpdate(){
//		AdSpace aSpace = aService.getAllById("8a5e9d1f5de5a6ee015de5a6f3ab0000");
//		AdSpace ad = aService.updateStaus(aSpace);
//		System.out.println("update..."+ad.toString());
//		Assert.assertNotNull(ad);
		
//		AdSpace aSpace2 = new AdSpace("8a5e9d1f5dd6bb8a015dd6bb8dd80009", 9999.99, AdSpaceStatusEnum.PASS);
//		AdSpace adSpace2 = aService.updateStaus(aSpace2);
//		System.out.println("update2..."+adSpace2);
//		Assert.assertNull(adSpace2);
	}
	
	@Test
	public void getPass(){
		List<AdSpaceToCDto> adSpaces = aService.getPassInTime(AdSpace.PASS, new Date());
		for(AdSpaceToCDto a:adSpaces){
			System.out.println("getpass..."+a.toString());
		}
	}
}
